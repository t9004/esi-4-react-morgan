# ESI-4-REACT-MORGAN

## Compte rendu :
J'avais déja quelques connaissances basiques en React mais j'ai appris de nouvelles choses comme l'utilisation des store avec le useContext et le useReducer. Ce me sera très utile pour mon projet de fin d'année que je fais en React.

Dans notre groupe, on a utilisé ChakraUI, j'ai bien aimé travailler avec cette librairie, je n'avais jamais utilisé de librairie de composants CSS et je trouve que c'est très pratique et beaucoup plus lisible que d'utiliser plein de classes différentes sur une meme balise. Très bonne découverte.


