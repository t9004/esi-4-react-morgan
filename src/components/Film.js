import style from './film.module.css'

function Film (props){
  const { film } = props;

  return (
    <>
    <div key={film.episode_id} className={style.card}>
      <div className={style.card_body}>
        <h1 className={style.title}>{ film.title }</h1>
        <p className={style.paragraph}>{ film.opening_crawl }</p>

        <div className={style.field}>
          <label>Directeur: </label>
          <span>{film.director}</span>
        </div>

        <div className={style.field}>
          <label>Producteur: </label>
          <span>{film.producer}</span>
        </div>

        <div className={style.field}>
          <label>Date: </label>
          <span>{film.releas_date}</span>
        </div>
      </div>
    </div>
    </>

  );
}

export default Film;