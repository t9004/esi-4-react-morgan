import React, { useEffect, useState } from 'react';
import Film from './components/Film';

function App() {
  const [films, setFilms] = useState([]);

  const updateFilms = () => {
    setFilms([]);
    fetch('https://swapi.dev/api/films/')
      .then(response => response.json())
      .then((response) => {
        setFilms(response.results)
      })
  }

  useEffect(() => {
    console.log('Changement de films')
  }, [films])

  return (
    <div>
      <button onClick={updateFilms}>Recharger les films</button>
      { films.map(f => <Film film={f} />) }
    </div>
  );
}

export default App;
